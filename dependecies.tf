provider "aws" {
    region = "XXXXXXXXXX"
    access_key = "XXXXXXXXXXXXXXXXXXXX"
    secret_key = "XXXXXXXXXXXXXXXXX"
}

resource "aws_instance" "db" {
    ami = "ami-XXXXXXXX"
    instance_type = "t2.micro"
}

resource "aws_instance" "web" {
    ami = "ami-XXXXXXXXXXXXX"
    instance_type = "t2.micro"

    depends_on = [aws_instance.db]
}