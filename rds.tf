provider "aws" {
    region = "XXXXXX"
    access_key = "XXXXXXXXXXXX"
    secret_key = "XXXXXXXXXXXX"
}

resource "aws_db_instance" "myRDS" {
    name = "myDB"
    identifier = "my-first-rds"
    instance_class = "db.t2.micro"
    engine = "mariadb"
    engine_version = "10.2.21"
    username = "XXXXX"
    password = "XXXXX"
    port = XXXXX
    allocated_storage = XXXX
    skip_final_snapshot = true
}