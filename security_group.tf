provider "aws" {
  region     = "XXXXX"
  access_key = "XXXXXXXXXXXXXXX"
  secret_key = "XXXXXXXXXXXXXXXX"
}

resource "aws_instance" "ec2" {
  ami           = "ami-XXXXXX" 
  instance_type = "t2.micro"
  security_groups = [aws_security_group.webtraffic.name]
}

resource "aws_security_group" "webtraffic" {
  name = "Allow HTTP"

  ingress {
    from_port = XXXX
    to_port = XXXX
    protocol = "TXXXX"
    cidr_blocks = ["XXXXX"]
  }
  egress {
    from_port = XXXX
    to_port = XXX
    protocol = "XXXX"
    cidr_blocks = ["XXXXX"]
  }
}