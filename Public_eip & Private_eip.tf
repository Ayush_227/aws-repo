provider "aws" {
  region     = "us-east-2"
  access_key = "XXXXXXXX"
  secret_key = "XXXXXXXXX"
}

resource "aws_instance" "db" {
    ami = "ami-XXXXXXX"
    instance_type = "t2.micro"

    tags = {
      Name = "DB Server"
    }
}

resource "aws_instance" "web" {
    ami = "ami-XXXXXXX"
    instance_type = "t2.micro"
    security_groups = [aws_security_group.Web_traffic.name]
    tags = {
        Name = "Web Server"
    }
}
resource "aws_eip" "web_ip" {
    instance = aws_instance.web.id
}

variable "ingress" {
    type = list (number)
    default = [XXXXX] 
}

variable "egress" {
    type = list (number)
    default = [XXXXX] 
}

resource "aws_security_group" "Web_traffic" {
    name = "Allow Web Traffic"

    dynamic "ingress" {
    iterator = port
    for_each = var.ingress
    content {
        from_port = port. value
        to_port = port. value
        protocol = "TCP"
        cidr_blocks = ["XXXXXX"]
     }
 }

    dynamic "egress" {
    iterator = port
    for_each = var.egress
    content {
        from_port = port. value
        to_port = port. value
        protocol = "TCP"
        cidr_blocks = ["XXXXX"]
    }
}
}

output "PrivateIP" {
    value = aws_instance.db.private_ip
}

output "PublicIP" {
    value = aws_eip.web_ip.public_ip
}

